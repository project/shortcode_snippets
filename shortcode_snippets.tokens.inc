<?php

/**
 * @file
 * Token hooks.
 */

/**
 * Implements hook_token_info().
 */
function shortcode_snippets_token_info() {
  $snippets = shortcode_snippets_load_all();

  $info['types']['shortcode-snippets'] = array(
    'name' => t('Shortcode snippets'),
    'description' => t('Tokens related to shortcode snippets.'),
  );

  foreach ($snippets as $snippet) {
    $info['tokens']['shortcode-snippets'][$snippet->id . ':' . $snippet->name] = array(
      'name' => $snippet->name,
      'description' => $snippet->description,
    );
  }

  return $info;
}

/**
 * Implements hook_tokens().
 */
function shortcode_snippets_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  $sanitize = !empty($options['sanitize']);

  if ($type == 'shortcode-snippets') {
    foreach ($tokens as $name => $original) {
      $snippet_id = explode(':', $name);
      $snippet = shortcode_snippets_load($snippet_id[0]);

      if ($snippet) {
        $replacements[$original] = check_markup($snippet->snippet, $snippet->format);
      }
    }
  }

  return $replacements;
}
