==== Description
Extensions for Shortcode module. This module allows you to create custom snippets.

==== Configuration
Go to admin/structure/shortcode-snippets and add snippet.

==== Dependencies
- Shortcode
- Token
- PHP module to use PHP code as snippet.

==== Sponsorship
This project is sponsored by Droptica - www.droptica.com
