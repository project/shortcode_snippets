<?php

/**
 * @file
 * Administration file.
 */

/**
 * List all snippets - page callback.
 */
function shortcode_snippets_list_page() {
  $data = shortcode_snippets_load_all();
  $data_table = array();

  if ($data) {
    // Prepare data for table.
    foreach ($data as $row) {
      $row = (array) $row;

      $snippet = mb_substr(htmlentities($row['snippet']), 0, 250, 'UTF-8');
      $row['snippet'] = '<code>' . $snippet . '</code>';

      $row['edit'] = l(t('Edit'), 'admin/structure/shortcode-snippets/' . $row['id'] . '/edit');
      $row['delete'] = l(t('Delete'), 'admin/structure/shortcode-snippets/' . $row['id'] . '/delete');

      $data_table[] = $row;
    }
  }

  $output = theme('table', array(
    'header' => shortcode_snippets_list_header(),
    'rows' => $data_table,
    'empty' => t('There is no snipets.'),
    'attributes' => array('width' => '100%')
  ));

  return $output;
}

/**
 * Get header for table.
 */
function shortcode_snippets_list_header() {
  $header = array(
    t('ID'),
    t('Name'),
    t('Description'),
    t('Snippet'),
    t('Format'),
    t('Edit'),
    t('Delete'),
  );

  return $header;
}

/**
 * Project Form (add,edit).
 */
function shortcode_snippets_form($form, &$form_state) {
  if (arg(4) == 'edit') {
    $snippet = shortcode_snippets_load(arg(3));

    $form['id'] = array(
      '#type' => 'value',
      '#value' => $snippet->id,
    );
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Name'),
    '#default_value' => isset($snippet) ? $snippet->name : '',
  );
  $form['description'] = array(
    '#type' => 'textfield',
    '#required' => FALSE,
    '#title' => t('Description'),
    '#default_value' => isset($snippet) ? $snippet->description : '',
  );
  $form['snippet'] = array(
    '#type' => 'text_format',
    '#base_type' => 'textarea',
    '#required' => TRUE,
    '#title' => t('Snippet'),
    '#format' => isset($snippet) ? $snippet->format : 'full_html',
    '#default_value' => isset($snippet) ? $snippet->snippet : '',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save snippet'),
  );

  return $form;
}

/**
 * Form submit.
 */
function shortcode_snippets_form_submit($form, &$form_state) {
  // Remove unneeded values.
  form_state_values_clean($form_state);
  $snippet = (object) $form_state['values'];
  $snippet->format = $snippet->snippet['format'];
  $snippet->snippet = $snippet->snippet['value'];

  if (isset($snippet->id) && is_numeric($snippet->id) && shortcode_snippets_load($snippet->id)) {
    drupal_write_record('shortcode_snippets', $snippet, array('id'));
    drupal_set_message(t("Snippet has been updated"));
  }
  else {
    drupal_write_record('shortcode_snippets', $snippet);
    drupal_set_message(t("New snippet has been saved"));
  }

  // Clear token cache - refresh available tokens.
  token_clear_cache();

  $form_state['redirect'] = 'admin/structure/shortcode-snippets';
}

/**
 * Delete confirmation form.
 */
function shortcode_snippets_delete_form($form, &$form_state) {
  $id = arg(3);
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $id,
  );
  $snippet = shortcode_snippets_load($id);

  return confirm_form($form,
    t('Are you sure you want to delete snippet %name?', array('%name' => $snippet->name)),
    'admin/structure/shortcode-snippets',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function shortcode_snippets_delete_form_submit($form, &$form_state) {
  $id = $form_state['values']['id'];
  shortcode_snippets_delete($id);

  // Clear token cache - refresh available tokens.
  token_clear_cache();

  drupal_set_message(t('Snippet %id deleted.', array('%id' => $id )));

  $form_state['redirect'] = 'admin/structure/shortcode-snippets';
}
